module Main

data Ty = TyInt | TyBool | TyFun Ty Ty

interTy : Ty -> Type    
interTy TyBool = Bool
interTy TyInt = Int
interTy (TyFun A B) = interTy A -> interTy B

main : IO ()
main = putStrLn "Hello world"
